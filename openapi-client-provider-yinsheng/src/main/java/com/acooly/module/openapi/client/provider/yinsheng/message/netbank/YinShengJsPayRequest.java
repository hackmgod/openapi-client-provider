package com.acooly.module.openapi.client.provider.yinsheng.message.netbank;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengJsPayInfo;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/1/8 19:16
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.WECHAT_JSPAY, type = ApiMessageType.Request)
public class YinShengJsPayRequest extends YinShengRequest{

    /**
     * 通知地址
     */
    @NotBlank
    private String notify_url;

    /**
     * 微信公众号支付业务信息
     */
    @ApiItem(value = "biz_content")
    private YinShengJsPayInfo yinShengJsPayInfo;
}
