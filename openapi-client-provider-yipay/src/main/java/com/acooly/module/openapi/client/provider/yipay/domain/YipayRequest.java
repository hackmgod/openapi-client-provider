/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-23 12:58 创建
 */
package com.acooly.module.openapi.client.provider.yipay.domain;

import com.acooly.core.utils.Ids;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * @author zhangpu 2018-01-23 12:58
 */
@Getter
@Setter
public class YipayRequest extends YipayMessage {

    @YipayAlias(value = "orgCode")
    private String orgCode;

    @YipayAlias(value = "reqSeq")
    private String reqSeq = Ids.oid();

    @YipayAlias(value = "reqIp")
    private String reqIp;

    @YipayAlias(value = "reqTime")
    private String reqTime = String.valueOf(new Date().getTime());

    @Length(max = 256)
    @YipayAlias(value = "returnUrl")
    private String returnUrl;

    /**
     * 异步回调地址
     * 服务器通知业务参数，请根据此做业务处理
     */
    @Length(max = 256)
    @YipayAlias(value = "notifyUrl")
    private String notifyUrl;
}
