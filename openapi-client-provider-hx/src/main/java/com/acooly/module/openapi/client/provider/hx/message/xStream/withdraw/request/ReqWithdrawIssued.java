package com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.request;


import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("IssuedDetails")
public class ReqWithdrawIssued {

    @XStreamAlias("Detail")
    private ReqWithdrawDetail detail;
}
