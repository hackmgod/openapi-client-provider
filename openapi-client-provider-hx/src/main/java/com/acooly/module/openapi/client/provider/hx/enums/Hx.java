package com.acooly.module.openapi.client.provider.hx.enums;

/**
 * 环讯参数定义类
 * @author SongCheng
 * 2015年9月17日下午4:15:15
 */
public class Hx {
	public static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(Hx.class);

	public static final String VERSION = "v1.0.0";


}//class
