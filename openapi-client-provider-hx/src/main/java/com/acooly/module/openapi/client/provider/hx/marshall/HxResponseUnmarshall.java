/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.hx.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.hx.HxProperties;
import com.acooly.module.openapi.client.provider.hx.domain.HxResponse;
import com.acooly.module.openapi.client.provider.hx.exception.ApiClientHxProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;

/**
 * @author zhangpu
 */
@Slf4j
@Service
public class HxResponseUnmarshall extends HxMarshallSupport implements ApiUnmarshal<HxResponse, String> {

    private static final Logger logger = LoggerFactory.getLogger(HxResponseUnmarshall.class);


    @Resource(name = "hxMessageFactory")
    private MessageFactory messageFactory;

    @Autowired
    protected HxProperties hxProperties;


    @SuppressWarnings("unchecked")
    @Override
    public HxResponse unmarshal(String message, String serviceName) {
        try {
            logger.info("响应报文:{}", message);
            HxResponse ylResponse = (HxResponse) messageFactory.getResponse (serviceName);
            doVerifySign(message,hxProperties.getPartnerId(),serviceName);
            ylResponse = (HxResponse)ylResponse.str2Obj(message,ylResponse,serviceName);
            return ylResponse;
        } catch (Exception e) {
            throw new ApiClientHxProcessingException("解析响应报文错误:" + e.getMessage());
        }
    }
}
