package com.acooly.module.openapi.client.provider.hx.exception;

/**
 * @author zhike 2018/2/6 13:21
 */
public class ApiClientHxProcessingException extends RuntimeException{

    private static final long serialVersionUID = 788548256305224365L;

    public ApiClientHxProcessingException() {
    }

    public ApiClientHxProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiClientHxProcessingException(String message) {
        super(message);
    }

    public ApiClientHxProcessingException(Throwable cause) {
        super(cause);
    }
}
