package com.acooly.module.openapi.client.provider.allinpay.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 19:41
 * @Description:
 */
@Getter
@Setter
@XStreamAlias("TRANSRET")
public class AllinpaySinglePaymentResponseBody implements Serializable {
    /**
     * 商户代码
     * 商户ID
     */
    @Size(max = 15)
    @NotBlank
    @XStreamAlias("MERCHANT_ID")
    private String merchantId;

    /**
     * 返回码
     */
    @Size(max = 4)
    @XStreamAlias("RET_CODE")
    private String retCode;

    /**
     * 清算日期
     * YYYYMMDD
     */
    @Size(max = 8)
    @NotBlank
    @XStreamAlias("SETTLE_DAY")
    private String settleDay;

    /**
     * 错误文本
     */
    @Size(max = 256)
    @XStreamAlias("ERR_MSG")
    private String errMsg;
}
