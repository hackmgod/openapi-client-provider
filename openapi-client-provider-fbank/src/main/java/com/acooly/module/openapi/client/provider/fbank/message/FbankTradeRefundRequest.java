package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankRequest;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-07 10:22
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_REFUND, type = ApiMessageType.Request)
public class FbankTradeRefundRequest extends FbankRequest {

    /**
     * 退款商户订单号
     * 商户自己平台的订单号
     */
    @NotBlank
    @Size(max = 64)
    private String mchntOrderNo;


    /**
     * 退款类型
     * 0全款，1部分
     */
    @NotBlank
    @Size(max = 1)
    private String refundType = "1";

    /**
     * 退款金额
     * 退款金额（分）
     */
    @NotBlank
    @Size(max = 12)
    private String refundFee;

    /**
     * 退款理由
     * 退款的具体原因
     */
    @NotBlank
    @Size(max = 50)
    private String reason;

    /**
     * 商户退款订单号
     * 商户自定义退款的订单号
     */
    @NotBlank
    @Size(max = 50)
    private String refMchntOrderNO;

    /**
     * 退款异步通知地址
     * 退款异步通知地址（针对银联有用）
     */
    @Size(max = 128)
    private String refundNotifyUrl;
}
