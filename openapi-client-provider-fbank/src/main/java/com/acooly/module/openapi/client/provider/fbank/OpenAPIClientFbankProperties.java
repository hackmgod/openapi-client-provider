/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.fbank;

import com.acooly.core.utils.Strings;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhike@acooly.cn
 */
@Getter
@Setter
@ConfigurationProperties(prefix = OpenAPIClientFbankProperties.PREFIX)
public class OpenAPIClientFbankProperties {

    public static final String PREFIX = "acooly.openapi.client.fbank";

    /**
     * 交易请求地址
     */
    private String payolGatewayUrl;

    /**
     * 其他接口请求地址
     */
    private String merGatewayUrl;

    /**
     * 银行加密公钥
     */
    private String publicKey;

    /**
     * 签名、验签秘钥
     */
    private String md5Key;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    /**
     * 商户号
     */
    private String partnerId;

    /**
     * 异步通知URL前缀
     */
    private String notifyUrlPrefix = "/";

    private String notifyUrl = "/gateway/notify/fbankNotify/";

    /**
     * 对账文件路径
     */
    private String filePath;

    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }
}
