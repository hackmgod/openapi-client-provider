/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.baofup.domain;

import com.acooly.core.utils.Dates;
import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author zhike
 */
@Getter
@Setter
public class BaoFuPApiMessage implements ApiMessage {

    /**
     * 服务码
     */
    @XStreamOmitField
    private String service;

    /**
     * 服务版本
     */
    @NotBlank(message = "服务版本不能为空")
    @BaoFuPAlias(value = "version")
    private String version = "4.0.0.0";

    /**
     * 终端号
     */
    @NotBlank(message = "终端号不能为空")
    @BaoFuPAlias(value = "terminal_id")
    private String terminalId;

    /**
     * 交易类型
     */
    @BaoFuPAlias(value = "txn_type")
    private String txnType;

    /**
     * 商户号
     */
    @BaoFuPAlias(value = "member_id")
    @NotBlank(message = "商户号不能为空")
    private String memberId;

    /**
     * 报文发送日期时间
     * 发送方发出本报文时的机器日期时间，如 2017-12-19 20:19:19
     */
    @NotBlank
    @BaoFuPAlias(value = "send_time")
    private String sendTime = Dates.format(new Date(),Dates.CHINESE_DATETIME_FORMAT_LINE);

    /**
     * 签名域(请求不用传)
     */
    @BaoFuPAlias(value = "signature",sign = false)
    private String signature;

    /**
     * 数字信封
     * 格式：01|对称密钥，01代表AES
     * 加密方式：Base64转码后使用宝付的公钥加密
     */
    @Size(max = 512)
    @BaoFuPAlias(value = "dgtl_envlp")
    private String dgtlEnvlp;

    /**
     * 商户保留域1
     */
    @Size(max = 255)
    @BaoFuPAlias(value = "req_reserved1")
    private String reqReservedOne;

    /**
     * 商户保留域2
     */
    @Size(max = 255)
    @BaoFuPAlias(value = "req_reserved2")
    private String reqReservedTwo;

    /**
     * 系统保留域1
     */
    @Size(max = 255)
    @BaoFuPAlias(value = "additional_info1")
    private String additionalInfoOne;

    /**
     * 系统保留域2
     */
    @Size(max = 255)
    @BaoFuPAlias(value = "additional_info2")
    private String additionalInfoTwo;

    /**
     * 请求url
     */
    @BaoFuPAlias(value = "gatewayUrl", sign = false, request = false)
    private String gatewayUrl;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public String getService() {
        return service;
    }

    @Override
    public String getPartner() {
        return memberId;
    }

    public void doCheck() {};
}
