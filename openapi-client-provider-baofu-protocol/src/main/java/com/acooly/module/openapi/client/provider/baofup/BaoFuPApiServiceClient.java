/**
 * create by zhike
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.baofup;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiClientSocketTimeoutException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPNotify;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPResponse;
import com.acooly.module.openapi.client.provider.baofup.exception.BaoFuPApiClientProcessingException;
import com.acooly.module.openapi.client.provider.baofup.marshall.*;
import com.acooly.module.openapi.client.provider.baofup.message.BaoFuPBillDowloadRequest;
import com.acooly.module.openapi.client.provider.baofup.message.BaoFuPBillDowloadResponse;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import com.acooly.module.openapi.client.provider.baofup.utils.BaoFuPSecurityUtil;
import com.acooly.module.openapi.client.provider.baofup.utils.HttpServletRequestUtil;
import com.acooly.module.openapi.client.provider.baofup.utils.StringHelper;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 * ApiService执行器
 *
 * @author zhike
 */
@Component("baofuPApiServiceClient")
@Slf4j
public class BaoFuPApiServiceClient
        extends AbstractApiServiceClient<BaoFuPRequest, BaoFuPResponse, BaoFuPNotify, BaoFuPNotify> {


    public static final String PROVIDER_NAME = "baofup";

    @Resource(name = "baoFuPHttpTransport")
    private Transport transport;

    @Resource(name = "baoFuPRequestMarshall")
    private BaoFuPRequestMarshall requestMarshal;

    @Resource(name = "baoFuPRedirectMarshall")
    private BaoFuPRedirectMarshall redirectMarshal;

    @Resource(name = "baoFuPSignMarshall")
    private BaoFuPSignMarshall signMarshall;

    @Autowired
    private BaoFuPResponseUnmarshall responseUnmarshal;
    @Autowired
    private BaoFuPNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private OpenAPIClientBaoFuPProperties openAPIClientBaoFuPProperties;

    @Override
    public BaoFuPResponse execute(BaoFuPRequest request) {
        try {
            beforeExecute(request);
            beforeCheckRequest(request);
            request.setDgtlEnvlp("01|"+openAPIClientBaoFuPProperties.getAesKey());
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            String responseMessage = getTransport().request(requestMessage, url).getBody();
            BaoFuPResponse t = responseUnmarshal.unmarshal(responseMessage, request.getService());
            t.setMemberId(request.getMemberId());
            t.setService(request.getService());
            afterExecute(t);
            return t;
        } catch (BaoFuPApiClientProcessingException pe) {
            log.error("解析响应报文异常：" + pe.getMessage(), pe);
            throw pe;
        } catch (ApiClientSocketTimeoutException ase) {
            log.error("响应超时异常：" + ase.getMessage(), ase);
            throw new BaoFuPApiClientProcessingException(ase.getMessage());
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            if(e instanceof ApiClientSocketTimeoutException || e instanceof BaoFuPApiClientProcessingException) {
                throw  e;
            } else {
                throw new ApiClientException("内部错误:" + e.getMessage());
            }
        }
    }


    /**
     * 下载对账文件
     * @param request
     * @return
     */
    public BaoFuPBillDowloadResponse billDownload(BaoFuPRequest request) {
        BaoFuPBillDowloadResponse response = new BaoFuPBillDowloadResponse();
        String status = "failure";
        String resMessage = "下载对账文件失败";
        OutputStream outputStream = null;
        try {
            beforeExecute(request);
            BaoFuPBillDowloadRequest billDownloadRequest = (BaoFuPBillDowloadRequest) request;
            if (Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(openAPIClientBaoFuPProperties.getGatewayUrl());
            }
            if(Strings.isBlank(request.getMemberId())) {
                request.setMemberId(openAPIClientBaoFuPProperties.getPartnerId());
            }
            if(Strings.isBlank(billDownloadRequest.getFilePath())) {
                ((BaoFuPBillDowloadRequest) request).setFilePath(openAPIClientBaoFuPProperties.getFilePath());
            }
            String url = request.getGatewayUrl();
            SortedMap<String, String> billRequestMap = getBillRequestMap(request);
            String requestMessage = StringHelper.getRequestStr(billRequestMap);
            log.info("请求报文: {}", requestMessage);
            String responseMessage = getTransport().request(requestMessage, url).getBody();
            Map<String,String> resMap = StringHelper.getParamsMap(responseMessage);
            String respCode = resMap.get("resp_code");
            String tespMes = resMap.get("resp_msg");
            if(!Strings.equals(respCode,"0000")) {
                resMessage = tespMes;
            } else {
                String respBody = resMap.get("resp_body");
                if(Strings.isBlank(respBody)) {
                    resMessage = "报文实体为空";
                }else {
                    byte[] respBodyByte = BaoFuPSecurityUtil.Base64DecodeByte(respBody);
                    InputStream dateInputStream = new ByteArrayInputStream(respBodyByte);
                    String filePath = billDownloadRequest.getFilePath();
                    if (Strings.isBlank(filePath)) {
                        filePath = openAPIClientBaoFuPProperties.getFilePath();
                    }
                    String fileName = billDownloadRequest.getFileName();
                    String fileSuffix = billDownloadRequest.getFileSuffix();
                    if (Strings.isBlank(fileName)) {
                        fileName = billDownloadRequest.getSettleDate() + "." + fileSuffix;
                    } else {
                        fileName = fileName + "." + fileSuffix;
                    }
                    File billFile = new File(filePath + File.separator + fileName);
                    if (!billFile.exists()) {
                        billFile.getParentFile().mkdirs();
                    }
                    int count = 0;
                    while (count == 0) {
                        count = dateInputStream.available();
                    }
                    byte[] b = new byte[count];
                    billFile.createNewFile();
                    outputStream = new FileOutputStream(billFile);
                    dateInputStream.read(b);
                    outputStream.write(b);
                    outputStream.flush();
                    outputStream.close();
                    dateInputStream.close();
                    status = "success";
                    resMessage = "对账文件下载成功";
                }
            }
            log.info("账期settleDate={}对账文件下载完毕", billDownloadRequest.getSettleDate());
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                log.info("输出流关闭失败:{}", e.getMessage());
            }
            response.setBizRespCode(status);
            response.setBizRespMsg(resMessage);
        }
        return response;
    }

    @Override
    protected ApiMarshal<String, BaoFuPRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<BaoFuPNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, BaoFuPRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<BaoFuPNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    public BaoFuPSignMarshall getSignMarshall() {
        return signMarshall;
    }

    public void setSignMarshall(BaoFuPSignMarshall signMarshall) {
        this.signMarshall = signMarshall;
    }

    @Override
    public ApiUnmarshal<BaoFuPResponse,String> getResponseUnmarshal() {
        return null;
    }

    public void setResponseUnmarshal(BaoFuPResponseUnmarshall responseUnmarshal) {
        this.responseUnmarshal = responseUnmarshal;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }

    public BaoFuPNotify notice(HttpServletRequest request, String serviceKey) {
        try {
            Map<String, String> notifyData = HttpServletRequestUtil.getNoticeDateMap(request);
            BaoFuPNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
            log.info("通知报文：{}", JSON.toJSONString(notify));
            return notify;
        } catch (ApiClientException oce) {
            log.warn("客户端:{}", oce.getMessage());
            throw oce;
        } catch (Exception e) {
            log.warn("内部错误:{}", e.getMessage());
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }
    /**
     * 获取对账请求map
     *
     * @param source
     * @return
     */
    protected SortedMap<String, String> getBillRequestMap(BaoFuPRequest source) {
        SortedMap<String, String> signData = Maps.newTreeMap();
        Set<Field> fields = Reflections.getFields(source.getClass());
        String key = null;
        Object value = null;
        for (Field field : fields) {
            value = Reflections.getFieldValue(source, field.getName());
            if (value == null) {
                continue;
            }
            BaoFuPAlias baofuAlias = field.getAnnotation(BaoFuPAlias.class);
            if (baofuAlias == null) {
                continue;
            } else {
                if (!baofuAlias.sign()) {
                    continue;
                }
                key = baofuAlias.value();
            }
            if (Strings.isNotBlank((String) value)) {
                signData.put(key, (String) value);
            }
        }
        return signData;
    }

    /**
     * 请求前参数校验
     * @param request
     */
    private void beforeCheckRequest(BaoFuPRequest request) {
        if (Strings.isBlank(request.getGatewayUrl())) {
            request.setGatewayUrl(openAPIClientBaoFuPProperties.getGatewayUrl());
        }
        if(Strings.isBlank(request.getMemberId())) {
            request.setMemberId(openAPIClientBaoFuPProperties.getPartnerId());
        }
        if(Strings.isBlank(request.getTerminalId())) {
            request.setTerminalId(openAPIClientBaoFuPProperties.getTerminalId());
        }
    }
}
