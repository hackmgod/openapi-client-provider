package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/17 19:00
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.QUERY_AGREEMENT,type = ApiMessageType.Request)
public class ShengpayQueryAgreementRequest extends ShengpayRequest {

    /**
     * 签约协议号，如果不为空，将返回该协议号对应的唯一协议
     */
    private String agreementNo;

    /**
     * 商户系统内针对会员的唯一标识
     */
    private String outMemberId;

    /**
     * 机构代码，如：ICBC
     */
    private String bankCode;

    /**
     * 银行卡类型，如：CR/DR
     */
    private String bankCardType;

    /**
     * 银行卡号
     */
    private String bankNo;
}
