package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankBalancePayConfirmRequestBody implements Serializable {

    /**
     * 合作方机构号（网商银行分配）
     */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 付款方商户号
     */
    @Size(max = 32)
    @XStreamAlias("PayerMerchantId")
    @NotBlank
    private String payerMerchantId;

    /**
     * 收款方ID
     */
    @Size(max = 32)
    @XStreamAlias("PayeeId")
    @NotBlank
    private String payeeId;
    
    /**
     * 收款方类型
     */
    @Size(max = 16)
    @XStreamAlias("PayeeType")
    @NotBlank
    private String payeeType;

    /**
     * 外部订单请求流水号
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;

    /**
     * 网商支付订单号
      */
    @Size(max = 64)
    @XStreamAlias("OrderNo")
    @NotBlank
    private String orderNo;

    /**
     * 订单金额(金额为分)
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种，默认CNY
     */
    @Size(max = 3)
    @XStreamAlias("Currency")
    @NotBlank
    private String currency = "CNY";

    /**
     * 短信动态码
     */
    @Size(max = 16)
    @XStreamAlias("SmsCode")
    @NotBlank
    private String smsCode;

}
