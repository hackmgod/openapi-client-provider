package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/22 16:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankPayQueryRequestBody implements Serializable {
    /**
     *合作方机构号（网商银行分配）
     */
    @Size(max = 64)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     *商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
     */
    @Size(max = 64)
    @XStreamAlias("MerchantId")
    @NotBlank
    private String merchantId;

    /**
     *外部交易号。支付交易合作方系统提交的交易号。
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;
}
