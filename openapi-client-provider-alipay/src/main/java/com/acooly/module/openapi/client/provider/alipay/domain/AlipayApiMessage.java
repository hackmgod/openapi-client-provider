/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.alipay.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.alipay.support.AlipayAlias;

import lombok.Getter;
import lombok.Setter;

/** @author zhangpu */
@Getter
@Setter
public class AlipayApiMessage implements ApiMessage {

	/** 接口名称 */
	private String service;
	
	/** 接口名称 */
	@AlipayAlias(value = "app_id")
	private String appId;

	@Override
	public String toString() {
		return ToString.toString(this);
	}

	@Override
	public String getService() {
		return this.service;
	}

	@Override
	public String getPartner() {
		return this.appId;
	}
}
