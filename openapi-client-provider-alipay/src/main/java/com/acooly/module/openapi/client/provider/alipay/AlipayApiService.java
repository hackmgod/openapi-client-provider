package com.acooly.module.openapi.client.provider.alipay;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.provider.alipay.enums.AlipayServiceEnum;
import com.acooly.module.openapi.client.provider.alipay.message.AlipayTradeAppRequest;
import com.acooly.module.openapi.client.provider.alipay.message.AlipayTradeAppResponse;
import com.acooly.module.openapi.client.provider.alipay.message.AlipayTradeQueryRequest;
import com.acooly.module.openapi.client.provider.alipay.message.AlipayTradeQueryResponse;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AlipayApiService {

	@Resource(name = "alipayClient")
	private AlipayClient client;
	@Resource(name = "alipayApiServiceClient")
	private AlipayApiServiceClient apiServiceClient;
	@Autowired
	private OpenAPIClientAlipayProperties openAPIClientAlipayProperties;

	/**
	 * 支付宝APP支付
	 *
	 * @param request
	 * @return
	 */
	public AlipayTradeAppResponse payAlipayTradeApp(AlipayTradeAppRequest request) {
		AlipayTradeAppResponse response = new AlipayTradeAppResponse();

		AlipayTradeAppPayRequest alipay_request = new AlipayTradeAppPayRequest();
		// 封装请求支付信息
		AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
		model.setOutTradeNo(request.getOutTradeNo());
		model.setSubject(request.getBody());
		model.setTotalAmount(request.getAmount().toString());
		model.setBody(request.getBody());
		model.setProductCode("QUICK_MSECURITY_PAY");
		model.setTimeoutExpress(request.getTimeoutExpress());
		alipay_request.setBizModel(model);
		// 设置异步通知地址
		alipay_request.setNotifyUrl(openAPIClientAlipayProperties.getDomain() + "/gateway/notify/alipayNotify/"
				+ AlipayServiceEnum.PAY_ALIPAY_TRADE_APP.getKey());
		// 设置同步地址
		alipay_request.setReturnUrl(null);
		try {
			AlipayTradeAppPayResponse alipayResponse = client.sdkExecute(alipay_request);
			if (Strings.isNotBlank(alipayResponse.getBody())){
				response.setCode(AlipayConstants.SUCCESS_CODE);
				response.setMsg(alipayResponse.getMsg());
				response.setSubMsg(alipayResponse.getSubMsg());
			}
			response.setPayInfo(alipayResponse.getBody());
			response.setTradeNo(alipayResponse.getTradeNo());
			response.setOutTradeNo(alipayResponse.getOutTradeNo());
			response.setAppId(request.getAppId());
			response.setService(request.getService());
			
		} catch (ApiServerException ose) {
            log.warn("服务器:{}",ose.getMessage());
            throw ose;
        } catch (Exception e) {
			log.error("服务器:{}",e.getMessage(), e);
			throw new ApiClientException("服务器:" + e.getMessage());
		}
		return response;
	}

	/**
	 * 订单查询
	 *
	 * @param request
	 * @return
	 */
	public AlipayTradeQueryResponse unifiedTradeQuery(AlipayTradeQueryRequest request) {
		AlipayTradeQueryResponse response = new AlipayTradeQueryResponse();
		
		com.alipay.api.request.AlipayTradeQueryRequest alipay_request = new com.alipay.api.request.AlipayTradeQueryRequest();
		
		AlipayTradeQueryModel model = new AlipayTradeQueryModel();
		model.setOutTradeNo(request.getOutTradeNo());
		model.setTradeNo(request.getTradeNo());
		alipay_request.setBizModel(model);
		try {
			com.alipay.api.response.AlipayTradeQueryResponse alipayResponse = client.execute(alipay_request);
			
			response.setCode(alipayResponse.getCode());
			response.setTradeNo(alipayResponse.getTradeNo());
			response.setOutTradeNo(alipayResponse.getOutTradeNo());
			response.setAppId(request.getAppId());
			response.setService(request.getService());
			response.setTradeStatus(alipayResponse.getTradeStatus());
			response.setTotalAmount(alipayResponse.getTotalAmount());
			response.setReceiptAmount(alipayResponse.getReceiptAmount());
		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		return response;
	}

	public static void main(String[] args) {
		String privateKey = "";
		String publicKey = "";
		try {
			AlipayClient client = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", "2018122062678131",
					privateKey, "json", "UTF-8", publicKey, "RSA2");

			AlipayTradeAppPayRequest alipay_request = new AlipayTradeAppPayRequest();
			// 封装请求支付信息
			AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
			model.setOutTradeNo(Ids.getDid());
			model.setSubject("测试支付");
			model.setTotalAmount("0.01");
			model.setBody("测试支付1分钱");
			model.setProductCode("QUICK_MSECURITY_PAY");
			alipay_request.setBizModel(model);
			// 设置异步通知地址
			alipay_request.setNotifyUrl("http://alipay.test.ihunlizhe.com/notify/sdk/alipay/notify.html");
			// 设置同步地址
			alipay_request.setReturnUrl(null);
			String payParams = null;
			try {
				payParams = client.sdkExecute(alipay_request).getBody();
			} catch (AlipayApiException e) {
				e.printStackTrace();
			}
			System.out.println("=========payParams:" + payParams);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
