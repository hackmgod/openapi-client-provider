package com.acooly.module.openapi.client.provider.yuejb;

/**
 * @author ouwen
 *
 */
public enum YueJBServiceKeys {

	Freeze("freezeQuotarating", "额度冻结"),
	Loan("sellOnCreditLend", "放款");

	private String key;
	private String val;

	private YueJBServiceKeys(String key, String val) {
		this.key = key;
		this.val = val;
	}

	public String getKey() {
		return key;
	}

	public String getVal() {
		return val;
	}

}
