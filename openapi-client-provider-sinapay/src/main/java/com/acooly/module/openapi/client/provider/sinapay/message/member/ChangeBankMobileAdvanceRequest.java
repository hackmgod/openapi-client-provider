package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhike 2018/7/10 15:29
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CHANGE_BANK_MOBILE_ADVANCE, type = ApiMessageType.Request)
public class ChangeBankMobileAdvanceRequest extends SinapayRequest {

    /**
     *修改银行预留手机获取的ticket
     */
    @NotEmpty
    @ApiItem(value = "ticket")
    private String ticket;

    /**
     *手机验证码
     */
    @NotEmpty
    @ApiItem(value = "valid_code")
    private String validCode;
}
