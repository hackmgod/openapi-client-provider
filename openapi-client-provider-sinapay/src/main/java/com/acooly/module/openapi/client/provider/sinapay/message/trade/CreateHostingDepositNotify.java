/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月4日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayNotify;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_HOSTING_DEPOSIT,type=ApiMessageType.Notify)
public class CreateHostingDepositNotify extends SinapayNotify {

	/**
	 * 商户网站唯一订单号或者交易原始凭证号
	 *
	 * 商户网站唯一订单号或者交易原始凭证号
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "outer_trade_no")
	private String outerTradeNo;

	/**
	 * 内部交易凭证号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "inner_trade_no")
	private String innerTradeNo;

	/**
	 * 充值状态
	 *
	 * 充值状态， 详见附录中的充值状态
	 */
	@NotEmpty
	@Size(max = 20)
	@ApiItem(value = "deposit_status")
	private String depositStatus;

	/**
	 * 充值金额
	 *
	 * 单位元，可以含小数点
	 */
	@MoneyConstraint
	@ApiItem(value = "deposit_amount")
	private Money depositAmount;

	/**
	 * 支付方式
	 *
	 * 参考4.3 交易结果通知 pay_method参数说明
	 */
	@Size(max = 1000)
	@ApiItem(value = "pay_method")
	private String payMethod;
}
