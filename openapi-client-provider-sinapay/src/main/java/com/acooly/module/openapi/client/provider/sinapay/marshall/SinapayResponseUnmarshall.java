/**
 * create by zhike
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.sinapay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhike
 * @date 2018-1-23
 */
@Slf4j
@Component
public class SinapayResponseUnmarshall extends SinapayAbstractMarshall implements ApiUnmarshal<SinapayResponse, String> {


    @SuppressWarnings("unchecked")
    @Override
    public SinapayResponse unmarshal(String message, String serviceName) {
        return doUnMarshall(message, serviceName, false);
    }


}
