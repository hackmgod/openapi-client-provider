package com.acooly.module.openapi.client.provider.cj.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.cj.domain.CjRequest;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class CjRedirectPostMarshall extends CjMarshallSupport implements ApiMarshal<PostRedirect, CjRequest> {

    @Override
    public PostRedirect marshal(CjRequest source) {

        return null;
    }

}
