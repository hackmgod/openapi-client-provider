package com.acooly.module.openapi.client.provider.cj.message;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cj.domain.CjApiMsgInfo;
import com.acooly.module.openapi.client.provider.cj.domain.CjRequest;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@CjApiMsgInfo(service = CjServiceEnum.CJ_REAL_DEDUCT_QUERY, type = ApiMessageType.Request)
public class CjRealQueryRequest extends CjRequest {
    /**
     * 用户ID
     */
    private String userId;
    /** 原交易请求号 */
    private String qryReqSn;

    /** 返回结果信息 */
    /** 手续费 */
    private Money charge;
    /** 企业账号 */
    private String corpAcctNo;
    /** 企业名称 */
    private String corpAcctName;
    /** 账号 */
    private String accountNo;
    /** 账户名称 */
    private String accountName;
    /** 金额 */
    private Money amount;
    /** 外部企业流水号 */
    private String corpFlowNo;
    /** 备注 */
    private String summary;
    /** 用途 */
    private String postscript;
    /** 订单状态 */
    private String status;
    /** 订单信息描述 */
    private String detail;

}
