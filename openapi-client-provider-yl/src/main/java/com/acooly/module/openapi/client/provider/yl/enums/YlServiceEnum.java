/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.yl.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务名称枚举
 *
 * @author
 */
public enum YlServiceEnum implements Messageable {

    YL_REAL_DEDUCT("ylRealDeduct","100004", "银联单笔代扣"),
    YL_BATCH_DEDUCT("ylBatchDeduct","100001", "银联批量代扣"),
    YL_REAL_DEDUCT_QUERY("ylRealDeductQuery","200001", "银联实时代扣结果查询"),
    YL_BATCH_DEDUCT_QUERY("ylBatchDeductQuery","200001", "银联批量代扣结果查询"),
    YL_BILL_DOWNLOAD("ylBillDownload","000000", "银联对账文件下载"),
    ;
    private final String code;
    private final String key;
    private final String message;

    private YlServiceEnum(String code,String key, String message) {
        this.code = code;
        this.message = message;
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public String getKey() {
        return key;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (YlServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YlServiceEnum find(String code) {
        for (YlServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param key 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static YlServiceEnum findByKey(String key) {
        for (YlServiceEnum status : values()) {
            if (status.getKey().equals(key)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YlServiceEnum> getAll() {
        List<YlServiceEnum> list = new ArrayList<YlServiceEnum>();
        for (YlServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YlServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }


}
