<!-- title: 富友支付SDK组件 -->
<!-- type: gateway -->
<!-- author: zhike -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-fuyou</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

* 直接采用fuyou的SDK，然后扩展异步通知处理
#使用方式：
###在自己工程注入FuyouApiService

###此sdk提供富友的网关直连支付、快捷充值、代付(提现)

###密钥加载
* 富友商户号是同一个，但是网关支付用一个密钥，快捷是用的另外的一个密钥，所以在配置的时候需要分开配置
   
###示例
* 本sdk中网关支付和快捷支付的异步通知地址固定值，所以调用的时候不需要传入notify_url：请参考com.acooly.module.openapi.client.provider.fuyou.OpenAPIClientFuyouConfigration.getFuyouApiSDKServlet
   * 网关支付：/gateway/notify/fuyouNotify/fuyouNetBank
   * 快捷首次支付：/gateway/notify/fuyouNotify/firstAgreementPay
   * 快捷协议支付：/gateway/notify/fuyouNotify/agreementPay
   * 接收异步通知的时候实现接口NotifyHandler，重新handleNotify和serviceKey方法，其中serviceKey方法需要绑定的key值在枚举FuyouServiceEnum.getCode()可以获取到
   快捷支付异步通知接收示例：
```java
    @Slf4j
    @Service
    public class FuyouFirstQuickCreateOrderNotifyService implements NotifyHandler{
    
        @Override
        public void handleNotify(ApiMessage notify) {
            FuyouFirstAgreementPayNotify agrpayGetCodeOrderNotify = (FuyouFirstAgreementPayNotify)notify;
            log.info("富有首次快捷支付异步通知报文：{}", JSON.toJSONString(agrpayGetCodeOrderNotify));
    
        }
    
        @Override
        public String serviceKey() {
            return FuyouServiceEnum.FIRST_QUICKPAY_CREATE_ORDER.getCode();
        }
    }
```

* 其中网关支付跳转通知，和异步通知参数一致所以复用了异步通知实体，需要自己创建一个controller来接收，请求的时候手动传入retrunUrl，然后通过方法com.acooly.module.openapi.client.provider.fuyou.FuyouApiService.notice
   将接收到的参数转化为异步通知实体
   网关支付跳转通知接收示例：
   
```java
   @Controller
   @RequestMapping("/gateway/redirect/fuyou")
   @Slf4j
   public class TestFuyouReturnController {
   
       @Autowired
       private FuyouApiService fuyouApiService;
   
       @RequestMapping("/netbankReturn")
       public void netBank(HttpServletRequest request, HttpServletResponse response) {
           FuyouNetBankNotify fuyouNetBankNotify = (FuyouNetBankNotify)fuyouApiService.notice(request, FuyouServiceEnum.FUYOU_NETBANK.getCode());
           log.info("富友同步通知报文：{}", JSON.toJSON(fuyouNetBankNotify));
       }
   }
```

* 代付（提现）的异步通知地址需要通知富友手动配置到富友系统地址为：http:/xxxx.xxx.xxx//gateway/notify/fuyouNotify/fuyouWithdraw,其中域名地址为对接系统的访问地址
   代付支付跳转通知接收示例：
```java
    @Slf4j
    @Service
    public class FuyouWithdrawNotifyService implements NotifyHandler{
    
        @Override
        public void handleNotify(ApiMessage notify) {
            FuyouWithdrawNotify withdrawNotify = (FuyouWithdrawNotify)notify;
            log.info("富有提现异步通知报文：{}", JSON.toJSONString(withdrawNotify));
    
        }
    
        @Override
        public String serviceKey() {
            return FuyouServiceEnum.FUYOU_WITHDRAW.getCode();
        }
    }
```

###接口的服务码和对应的唯一表示统一放在com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum枚举里面：
* code:表示本组件定义的接口唯一标识
* key:为每个接口请求的后缀，方便拼装接口请求地址
* type:标注接口请求报文组装类型
* signKey:标注接口验签的时候取的签名串对应的key
* message:标注接口名称

<font color='#DC143C'>注意：</font>调用接口的时候只需要传入响应的业务参数，公共参数如商户号，接口的版本号非特殊情况不需要手动传入，charset默认为UTF-8
     配置文件方式不用传入请求地址

###sdk提供了单独签名、验签、异步实体转化方法

###配置文件示例：
* 组建是否启用标识
acooly.openapi.client.fuyou.enable=true
* 网关请求地址根路径
acooly.openapi.client.fuyou.netbankPayUrl=https://www-1.fuiou.com:28028/wg1_run
* 快捷请求地址根路径
acooly.openapi.client.fuyou.quickPayUrl=http://www-1.fuiou.com:18670/mobile_pay
* 代付(提现)请求地址根路径
acooly.openapi.client.fuyou.withdrawUrl=https://fht-test.fuiou.com/fuMer/req.do
* 快捷请求MD5密钥
acooly.openapi.client.fuyou.quickMd5Key=5old71wihg2tqjug9kkpxnhx9hiujoqj
* 网关和代付请求密钥
acooly.openapi.client.fuyou.netbankMd5Key=vau6p7ldawpezyaugc0kopdrrwm4gkpu
* 调用系统访问根路径
acooly.openapi.client.fuyou.domain=http://218.70.106.250:8881
* 请求连接超时时间
acooly.openapi.client.fuyou.connTimeout=10000
* 响应超时时间
acooly.openapi.client.fuyou.readTimeout=30000
* 测试环境网关支付商户号
acooly.openapi.client.fuyou.partnerId=0001000F0040992
* 测试环境快捷支付商户号
acooly.openapi.client.fuyou.partnerId=0002900F0096235
* 测试环境代付支付商户号
acooly.openapi.client.fuyou.partnerId=0002900F0345178
