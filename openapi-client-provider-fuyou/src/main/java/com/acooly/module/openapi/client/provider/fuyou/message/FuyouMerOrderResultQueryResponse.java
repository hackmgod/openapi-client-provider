package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.MER_ORDER_RESULT_QUERY,type = ApiMessageType.Response)
@XStreamAlias("RESPONSE")
public class FuyouMerOrderResultQueryResponse extends FuyouResponse{

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @XStreamAlias("MCHNTORDERID")
    private String merchOrderNo;

    /**
     * 交易金额
     * 交易金额，分为单位
     */
    @XStreamAlias("AMT")
    @NotBlank
    @Size(max = 12)
    private String amount;



    /**
     * 交易日期
     * 富友接受商户订单请求的时间（版
     * 本号传 2.0 时按富友接收订单时间
     * 返回）
     */
    @XStreamAlias("ORDERDATE")
    @Size(max = 8)
    private String orderDate;

    /**
     * 银行卡号
     * 请求报文中的银行卡号（版本号传
     * 2.0 时按请求参数返回）
     */
    @XStreamAlias("BANKCARD")
    @NotBlank
    @Size(max = 20)
    private String bankCard;



    /**
     * 摘要数据
     */
    @XStreamAlias("SIGN")
    private String sign;

    @Override
    public String getSignStr() {
        return getVersion()+"|"+getResponseCode()+"|"+getResponseMsg()+"|"+getMerchOrderNo()+"|";
    }
}
