package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.AGREEMENT_UNBUNDLING,type = ApiMessageType.Response)
@XStreamAlias("RESPONSE")
public class FuyouAgreementUnbundlingResponse extends FuyouResponse{

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 签名类型
     */
    @XStreamAlias("SIGNTP")
    private String signType;

    /**
     * 摘要数据
     */
    @XStreamAlias("SIGN")
    private String sign;
}
