/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.fuyou.marshall;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.fuyou.FuyouConstants;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouRespCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

/**
 * 跳转接口 同步跳回报文解析
 *
 * @author zhangpu
 */
@Service
public class FuyouReturnUnmarshall extends FuyouMarshallSupport
        implements ApiUnmarshal<FuyouResponse, Map<String, String>> {

    private static final Logger logger = LoggerFactory.getLogger(FuyouReturnUnmarshall.class);
    
    @Resource(name = "fuyouMessageFactory")
    private MessageFactory messageFactory;

    @SuppressWarnings("unchecked")
    @Override
    public FuyouResponse unmarshal(Map<String, String> message, String serviceName) {
        try {
            logger.info("同步通知{}", message);
            String signature = message.get("signature");
            String plain = getWaitForSign(message);
//            getSignerFactory().getSigner(FuyouConstants.SIGNER_KEY).verify(signature, getKeyPair(), plain);
            return doUnmarshall(message, serviceName);
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }

    }

    protected FuyouResponse doUnmarshall(Map<String, String> message, String serviceName) {
        FuyouResponse response = (FuyouResponse) messageFactory.getReturn(serviceName);
        Set<Field> fields = Reflections.getFields(response.getClass());
        String key = null;
        for (Field field : fields) {
            FuyouAlias fuyouAlias = field.getAnnotation(FuyouAlias.class);
            if (fuyouAlias == null) {
                continue;
            }
            key = fuyouAlias.value();
            if (Strings.isBlank(key)) {
                key = field.getName();
            }
            Reflections.invokeSetter(response, field.getName(), message.get(key));
        }
        response.setService(serviceName);
        if (Strings.isNotBlank(response.getRespCode())) {
            response.setMessage(FuyouRespCodes.getMessage(response.getRespCode()));
        }
        return response;
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }

}
