package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.MERSUPPORT_CARDBIN_QUERY,type = ApiMessageType.Response)
@XStreamAlias("FM")
public class FuyouMerSupportCardBinQueryResponse extends FuyouResponse{

    /**
     * 响应代码
     * 0000 表示“成功”
     * 1014 表示“无效卡号”
     * 5505 表示“不支持的银行卡”
     * 100001 表示“不支持的卡类型”
     */
    @XStreamAlias("Rcd")
    @NotBlank
    private String rcd;

    /**
     * 响应代码的中文描述
     */
    @XStreamAlias("RDesc")
    @Size(max = 40)
    @NotBlank
    private String rDesc;

    /**
     * 卡类型
     * 01-借记卡，02-信用卡，03-准贷记
     * 卡，04-富友卡，05-非法卡号
     */
    @Size(max = 2)
    @XStreamAlias("Ctp")
    private String cTp;

    /**
     * 银行名称
     * 银行卡所在的银行名称
     *
     */
    @XStreamAlias("Cnm")
    @Size(max = 40)
    private String cNm;

    /**
     * 银行名称
     * 银行卡所在的银行名称
     * 根据机构号前 6 位判断为同一家银
     * 行(北京银行除外，北京银行根据
     * 10 位机构号判断为同一家银行)
     * '0801020000' - 中国工商银行
     * '0801030000' - 中国农业银行
     * '0801040000' –中国银行
     * '0801050000' - 中国建设银行
     * '0804031000' –北京银行
     */
    @XStreamAlias("InsCd")
    private String insCd;


    /**
     * 签名类型
     */
    @XStreamAlias("SIGNTP")
    private String signType;

    /**
     * 摘要数据
     */
    @XStreamAlias("SIGN")
    private String sign;

    @Override
    public String getSignStr() {
        return getRcd()+"|";
    }
}
