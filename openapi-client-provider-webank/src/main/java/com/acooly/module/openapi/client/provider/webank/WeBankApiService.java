/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.webank;

import com.acooly.module.openapi.client.provider.webank.domain.WeBankNotify;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;
import com.acooly.module.openapi.client.provider.webank.message.WeBankDeductQueryRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankDeductQueryResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankDeductRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankDeductResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankSignRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankSignResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankSmsRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankSmsResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankWithdrawQueryRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankWithdrawQueryResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankWithdrawRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankWithdrawResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Service
public class WeBankApiService {
	
	@Resource(name = "weBankApiServiceClient")
	private WeBankApiServiceClient weBankApiServiceClient;

	@Autowired
	private WeBankProperties weBankProperties;

	/**
	 * 手机短信验证
	 * @param request
	 * @return
	 */
	public WeBankSmsResponse weBankSms(WeBankSmsRequest request) {
		request.setBizType(WeBankServiceEnum.WEBANK_SMS.code());
		return (WeBankSmsResponse)weBankApiServiceClient.execute(request);
	}

	/**
	 * 签约
	 * @param request
	 * @return
	 */
	public WeBankSignResponse weBankSign(WeBankSignRequest request) {
		request.setBizType(WeBankServiceEnum.WEBANK_SIGN.code());
		return (WeBankSignResponse)weBankApiServiceClient.execute(request);
	}

	/**
	 * 代扣
	 * @param request
	 * @return
	 */
	public WeBankDeductResponse weBankDeduct(WeBankDeductRequest request) {
		request.setBizType(WeBankServiceEnum.WEBANK_DEDUCT.code());
		request.getWeBankDeductInfo().setNotifyUrl(weBankProperties.getDomain()+"/gateway/notify/wzNotify");
		return (WeBankDeductResponse)weBankApiServiceClient.execute(request);
	}

	/**
	 * 代付
	 * @param request
	 * @return
	 */
	public WeBankWithdrawResponse weBankWithdraw(WeBankWithdrawRequest request) {
		request.getWeBankWithdrawInfo().setNotifyUrl(weBankProperties.getDomain()+"/gateway/notify/wzNotify");
		request.setBizType(WeBankServiceEnum.WEBANK_WITHDRAW.code());
		return (WeBankWithdrawResponse)weBankApiServiceClient.execute(request);
	}

	/**
	 * 代付查询
	 * @param request
	 * @return
	 */
	public WeBankWithdrawQueryResponse weBankWithdrawQuery(WeBankWithdrawQueryRequest request) {
		request.setBizType(WeBankServiceEnum.WEBANK_WITHDRAW_QUERY.code());
		return (WeBankWithdrawQueryResponse)weBankApiServiceClient.execute(request);
	}

	/**
	 * 代扣查询
	 * @param request
	 * @return
	 */
	public WeBankDeductQueryResponse weBankDeductQuery(WeBankDeductQueryRequest request) {
		request.setBizType(WeBankServiceEnum.WEBANK_DEDUCT_QUERY.code());
		return (WeBankDeductQueryResponse)weBankApiServiceClient.execute(request);
	}

	/**
	 * 解析异步通知
	 *
	 * @param request
	 * @param serviceKey
	 * @return
	 */
	public WeBankNotify notice(HttpServletRequest request, String serviceKey) {
		return  weBankApiServiceClient.notice(request,serviceKey);
	}

}
