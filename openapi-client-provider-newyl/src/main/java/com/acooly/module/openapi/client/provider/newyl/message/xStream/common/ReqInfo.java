package com.acooly.module.openapi.client.provider.newyl.message.xStream.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author fufeng 2018/1/26 15:26.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("INFO")
public class ReqInfo {
    /**
     *交易代码
     */
    @XStreamAlias("TRX_CODE")
    private String trxCode;
    /**
     *版本
     */
    @XStreamAlias("VERSION")
    private String version;
    /**
     *数据格式
     */
    @XStreamAlias("DATA_TYPE")
    private String dataType;
    /**
     *处理级别
     */
    @XStreamAlias("LEVEL")
    private String level;
    /**
     *用户名
     */
    @XStreamAlias("USER_NAME")
    private String userName;
    /**
     *USER_PASS
     */
    @XStreamAlias("USER_PASS")
    private String userPass;
    /**
     *交易流水号
     */
    @XStreamAlias("REQ_SN")
    private String reqSn;

    /**
     *签名信息
     */
    @XStreamOmitField
    private String signedMsg;

}
