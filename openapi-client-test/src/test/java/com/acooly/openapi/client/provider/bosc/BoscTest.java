package com.acooly.openapi.client.provider.bosc;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.provider.bosc.BoscApiService;
import com.acooly.module.openapi.client.provider.bosc.BoscFundApiService;
import com.acooly.module.openapi.client.provider.bosc.BoscTradeApiService;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscTransactionTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscCheckFileTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscRepaymentWayEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscTradeTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.AsyncTransactionRequest;
import com.acooly.module.openapi.client.provider.bosc.message.fund.AsyncTransactionResponse;
import com.acooly.module.openapi.client.provider.bosc.message.fund.ConfirmCheckFileRequest;
import com.acooly.module.openapi.client.provider.bosc.message.fund.ConfirmCheckFileResponse;
import com.acooly.module.openapi.client.provider.bosc.message.fund.DownloadCheckFileRequest;
import com.acooly.module.openapi.client.provider.bosc.message.fund.DownloadCheckFileResponse;
import com.acooly.module.openapi.client.provider.bosc.message.fund.QueryTransactionRechargeResponse;
import com.acooly.module.openapi.client.provider.bosc.message.fund.QueryTransactionRequest;
import com.acooly.module.openapi.client.provider.bosc.message.fund.QueryTransactionWithdrawResponse;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.BizDetailInfo;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.ConfirmCheckInfo;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.DetailInfo;
import com.acooly.module.openapi.client.provider.bosc.message.member.QueryUserInfomationRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.QueryUserInfomationResponse;
import com.acooly.module.openapi.client.provider.bosc.message.trade.EstablishProjectRequest;
import com.acooly.module.openapi.client.provider.bosc.message.trade.EstablishProjectResponse;
import com.acooly.module.openapi.client.provider.bosc.message.trade.UserAutoPreTransactionRequest;
import com.acooly.module.openapi.client.provider.bosc.message.trade.UserAutoPreTransactionResponse;
import com.acooly.test.NoWebTestBase;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhangpu@yiji.com
 */
@SpringBootApplication
@BootApp(sysName = "test")
public class BoscTest extends NoWebTestBase {

    @Autowired
    private BoscApiService boscApiService;
    
    @Autowired
    private BoscTradeApiService tradeApiService;
    
    @Autowired
    private BoscFundApiService fundApiService;


    /**
     * 查询用户信息
     */
    @Test
    public void testQueryUserInfomation() {
        QueryUserInfomationRequest request = new QueryUserInfomationRequest();
        request.setPlatformUserNo("liubin_b_1");
        QueryUserInfomationResponse response = boscApiService.queryUserInfomation(request);
        System.out.println(response);

    }
    
    
    @Test
    public void testUserAutoPreTransction(){
        UserAutoPreTransactionRequest request = new UserAutoPreTransactionRequest (Ids.oid (),"liubinttest",BoscBizTypeEnum.TENDER,new Money (5000),"TENDER17092722050732450002");
        UserAutoPreTransactionResponse response = tradeApiService.userAutoPreTransaction (request);
        Assert.assertTrue (response.getCode ().equals ("0") && BoscStatusEnum.SUCCESS.equals (response.getStatus ()));
    }
    
    @Test
    public void testEstablishProject(){
        EstablishProjectRequest request = new EstablishProjectRequest (Ids.oid (),"liubin_b_1",Ids.getDid ("TENDER"),new Money (500000),"刘斌的第一个测试标的");
        request.setRepaymentWay (BoscRepaymentWayEnum.ONE_TIME_SERVICING);
        request.setAnnnualInterestRate ("0.14");
        EstablishProjectResponse response = tradeApiService.establishProject (request);
        System.out.println (response);
    }
    
    
    @Test
    public void testAsyncTransaction(){
        AsyncTransactionRequest request = new AsyncTransactionRequest ();
        request.setBatchNo (Ids.oid ());
    
        List<BizDetailInfo> bizDetailInfos = Lists.newArrayList ();
        BizDetailInfo bizDetailInfo = new BizDetailInfo ();
        bizDetailInfo.setTradeType (BoscTradeTypeEnum.MARKETING);
        bizDetailInfo.setRequestNo (Ids.oid ());
        
        List<DetailInfo> detailInfos = Lists.newArrayList ();
        
        DetailInfo detailInfo = new DetailInfo ();
        detailInfo.setAmount (new Money (20));
        detailInfo.setBizType (BoscBizTypeEnum.MARKETING);
        detailInfo.setSourcePlatformUserNo ("SYS_GENERATE_002");
        detailInfo.setTargetPlatformUserNo ("liubin_b_1");
        detailInfos.add (detailInfo);
        
        bizDetailInfo.setDetails (detailInfos);
        
        bizDetailInfos.add (bizDetailInfo);
        
        request.setBizDetails (bizDetailInfos);
        
        AsyncTransactionResponse response = fundApiService.asyncTransaction (request);
        
        System.out.println (response);
    }
    
    
    @Test
    public void testDownloadCheckFile(){
        DownloadCheckFileRequest request = new DownloadCheckFileRequest ();
        request.setFileDate (DateFormatUtils.format (Dates.addDay (new Date (), -1),"yyyyMMdd"));
        request.setFilePath ("/Users/liunim/var/log/webapps/checkFile");
        
        DownloadCheckFileResponse response = fundApiService.downloadCheckFile (request);
    
        System.out.println (response);
    }
    
    @Test
    public void testConfirmCheckFile(){
        ConfirmCheckFileRequest request = new ConfirmCheckFileRequest ();
        request.setRequestNo (Ids.getDid ());
        request.setFileDate (DateFormatUtils.format (Dates.addDay (new Date (),-1),"yyyyMMdd"));
        
        ConfirmCheckFileResponse response = fundApiService.confirmCheckFile (request);
        System.out.println (response);
    }
    
    @Test
    public void  testRechargeQuery(){
        QueryTransactionRequest request = new QueryTransactionRequest ();
        request.setRequestNo ("17101317230329650100");
        request.setTransactionType (BoscTransactionTypeEnum.RECHARGE);
        
        QueryTransactionRechargeResponse response = fundApiService.queryTransactionRecharge (request);
        System.out.println (response);
    }
    
    @Test
    public void testWithdrawQuery(){
        QueryTransactionRequest request = new QueryTransactionRequest ();
        request.setRequestNo ("17101613493258690062");
        request.setTransactionType (BoscTransactionTypeEnum.WITHDRAW);
    
        QueryTransactionWithdrawResponse response = fundApiService.queryTransactionWithdraw (request);
        
    }
}
