package com.acooly.openapi.client.provider.fuiou;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.fuiou.FuiouApiService;
import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouResponse;
import com.acooly.module.openapi.client.provider.fuiou.message.FuiouRegRequest;
import com.acooly.test.NoWebTestBase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author qiubo@yiji.com
 */
@SpringBootApplication
@BootApp(sysName = "test")
public class FuiouTest extends NoWebTestBase {
    @Autowired
    private FuiouApiService fuiouApiService;

    private String mchntCd = "0002900F0096235";
    @Test
    public void name() throws Exception {
        assertThat(fuiouApiService).isNotNull();
    }


    @Test
    public void testReg() {
        //已注册用户 13996039896 13036355227
        FuiouRegRequest reqData = new FuiouRegRequest();
        reqData.setPartner(mchntCd);
        reqData.setRequestNo(Ids.oid());
        reqData.setCustNm("zhangsan");
        reqData.setCertifTp("0");
        reqData.setCertifId("510823197307160317");
        reqData.setMobileNo("13036355227");
        reqData.setEmail("bingzhiwawa@163.com");
        // 未找到代码表，直接填写默认值：1000
        reqData.setCityId("1000");
        // 代码来自：富友银行代码表20160217.xlsx
        reqData.setParentBankId("0305");
        // reqData.setBank_nm("招商银行股份有限公司上海联洋支行");
        reqData.setCapAcntNm("张三");
        reqData.setCapAcntNo("6226221100445060");
        FuiouResponse response = fuiouApiService.reg(reqData);
        System.out.println(response);
    }
}
