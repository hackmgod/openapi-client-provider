package com.acooly.openapi.client.provider.hx;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.hx.HxApiService;
import com.acooly.module.openapi.client.provider.hx.message.HxNetbankPayQueryRequest;
import com.acooly.module.openapi.client.provider.hx.message.HxNetbankPayQueryResponse;
import com.acooly.module.openapi.client.provider.hx.message.HxNetbankPayRequest;
import com.acooly.module.openapi.client.provider.hx.message.HxNetbankPayResponse;
import com.acooly.module.openapi.client.provider.hx.message.HxWithdrawQueryRequest;
import com.acooly.module.openapi.client.provider.hx.message.HxWithdrawQueryResponse;
import com.acooly.module.openapi.client.provider.hx.message.HxWithdrawRequest;
import com.acooly.module.openapi.client.provider.hx.message.HxWithdrawResponse;
import com.acooly.module.openapi.client.provider.hx.message.xStream.common.ReqHead;
import com.acooly.module.openapi.client.provider.hx.message.xStream.common.ReqWithdrawHead;
import com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPay.request.GateWayReq;
import com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPay.request.ReqNetBankBody;
import com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPayQuery.request.OrderQueryReq;
import com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPayQuery.request.ReqNetBankQueryBody;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.request.ReqWithdrawBody;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.request.ReqWithdrawDetail;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.request.ReqWithdrawIssued;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.request.IssuedTradeReq;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.request.ReqWithdrawQueryBody;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "YlTest")
public class HxTest extends NoWebTestBase {

    @Autowired
    private HxApiService hxApiService;

    String oid = Ids.gid();

    /**
     * 环讯网银测试
     */
    @Test
    public void testHxNetbankPay() {

        HxNetbankPayRequest request = new HxNetbankPayRequest();

        GateWayReq gateWayReq = new GateWayReq();

        ReqHead reqHead = new ReqHead();

        reqHead.setMerCode("158964");
        reqHead.setMerName("IPS-营销中心-内测03");
        reqHead.setAccount("1589640018");
        reqHead.setMsgId("msg20180301103304");
        reqHead.setReqDate("20180301103304");

        ReqNetBankBody reqNetBankBody = new ReqNetBankBody();

        reqNetBankBody.setMerBillNo("");
        reqNetBankBody.setAmount("");
        reqNetBankBody.setDate("");
        reqNetBankBody.setCurrencyType("");
        reqNetBankBody.setGatewayType("");
        reqNetBankBody.setLang("");
        reqNetBankBody.setMerchanturl("");
        reqNetBankBody.setFailUrl("");
        reqNetBankBody.setAttach("");
        reqNetBankBody.setOrderEncodeType("");
        reqNetBankBody.setRetEncodeType("");
        reqNetBankBody.setRetType("");
        reqNetBankBody.setServerUrl("");
        reqNetBankBody.setBillEXP("");
        reqNetBankBody.setGoodsName("");
        reqNetBankBody.setIsCredit("");
        reqNetBankBody.setBankCode("");
        reqNetBankBody.setProductType("");
        reqNetBankBody.setUserRealName("");
        reqNetBankBody.setUserId("");
        reqNetBankBody.setCardInfo("");

        gateWayReq.setHead(reqHead);
        gateWayReq.setBody(reqNetBankBody);
        request.setGateWayReq(gateWayReq);

        try {
            HxNetbankPayResponse response = hxApiService.hxNetbankPay(request);
            System.out.println("环讯网银测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * 环讯网银查询测试
     */
    @Test
    public void testHxNetbankPayQuery() {

        HxNetbankPayQueryRequest request = new HxNetbankPayQueryRequest();

        request.setGatewayUrl("https://newpay.ips.com.cn/psfp-entry/services/order?wsdl");
        request.setNamespaceUrl("http://payat.ips.com.cn/WebService/OrderQuery");
        request.setLocalPart("getOrderByMerBillNo");
        request.setParamName("orderQuery");

        OrderQueryReq orderQueryReq = new OrderQueryReq();

        ReqHead reqHead = new ReqHead();

        reqHead.setMerCode("158964");
        reqHead.setAccount("1589640018");
        reqHead.setReqDate("20180301103304");

        ReqNetBankQueryBody reqNetBankQueryBody = new ReqNetBankQueryBody();

        reqNetBankQueryBody.setMerBillNo("o18030216521554100002");
        reqNetBankQueryBody.setDate("20180302");
        reqNetBankQueryBody.setAmount("20.00");

        orderQueryReq.setHead(reqHead);
        orderQueryReq.setBody(reqNetBankQueryBody);
        request.setOrderQueryReq(orderQueryReq);

        try {
            HxNetbankPayQueryResponse response = hxApiService.hxNetbankPayQuery(request);
            System.out.println("环讯网银测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * 代付测试
     */
    @Test
    public void testHxWithdraw() {
        HxWithdrawRequest request = new HxWithdrawRequest();

        request.setGatewayUrl("https://merservice.ips.com.cn/pfas-merws/services/issued?wsdl");
        request.setNamespaceUrl("http://webservice.ips.com.cn");
        request.setLocalPart("issued");
        request.setParamName("arg0");

        ReqWithdrawDetail detail = new ReqWithdrawDetail();

        detail.setMerBillNo("o18030111395272140004");
        detail.setAccountName("<![CDATA[韦崇凯]]>");
        detail.setAccountNumber("6217711201085972");
        detail.setBankName("<![CDATA[中信银行]]>");
        detail.setBranchBankName("<![CDATA[渝北支行]]>");
        detail.setBankCity("<![CDATA[重庆]]>");
        detail.setBankProvince("<![CDATA[重庆]]>");
        detail.setBillAmount("3.00");
        detail.setIdCard("500221198810192313");
        detail.setMobilePhone("18696725229");

        ReqWithdrawBody body = new ReqWithdrawBody();

        body.setBizId("1");
        body.setChannelId("2");
        body.setCurrency("156");
        body.setDate("20180301103304");
        body.setAttach("<![CDATA[下发]]>");

        ReqWithdrawIssued issued = new ReqWithdrawIssued();

        issued.setDetail(detail);

        body.setIssuedDetails(issued);

        ReqWithdrawHead head = new ReqWithdrawHead();

        head.setMerCode("158964");
        head.setMerName("IPS-营销中心-内测03");
        head.setAccount("1589640018");
        head.setMsgId("msg20180301103304");
        head.setReqDate("20180301103304");
        request.setReqWithdrawHead(head);

        request.setReqWithdrawBody(body);

        try {
            HxWithdrawResponse response = hxApiService.hxWithdraw(request);
            System.out.println("环讯代付测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * 代付查询测试
     */
    @Test
    public void testHxWithdrawQuery() {
        HxWithdrawQueryRequest request = new HxWithdrawQueryRequest();

        request.setGatewayUrl("https://query.ips.com.cn/psfp-wsquery/services/trade?wsdl");
        request.setNamespaceUrl("http://payat.ips.com.cn/WebService/TradeQuery");
        request.setLocalPart("getIssuedByBillNo");
        request.setParamName("getIssuedByBillNo");

        IssuedTradeReq issuedTradeReq = new IssuedTradeReq();

        ReqHead reqHead = new ReqHead();

        reqHead.setMerCode("158964");
        reqHead.setMerName("IPS-营销中心-内测03");
        reqHead.setAccount("1589640018");
        reqHead.setMsgId("msg20180301103304");
        reqHead.setReqDate("20180301103304");

        ReqWithdrawQueryBody reqWithdrawQueryBody = new ReqWithdrawQueryBody();

        issuedTradeReq.setHead(reqHead);
        issuedTradeReq.setBody(reqWithdrawQueryBody);

        reqWithdrawQueryBody.setBatchNo("BA170320180228103305001999");
        reqWithdrawQueryBody.setMerBillNo("o18022810330438420004");

        request.setIssuedTradeReq(issuedTradeReq);

        try {
            HxWithdrawQueryResponse response = hxApiService.hxWithdrawQuery(request);
            System.out.println("环讯代付查询测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

}