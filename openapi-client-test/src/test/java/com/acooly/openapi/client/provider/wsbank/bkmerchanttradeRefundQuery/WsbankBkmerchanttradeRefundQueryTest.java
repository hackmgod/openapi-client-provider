package com.acooly.openapi.client.provider.wsbank.bkmerchanttradeRefundQuery;

import com.acooly.core.common.BootApp;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradeRefundQueryRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradeRefundQueryResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradeRefundQueryRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradeRefundQueryRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "wsbankBkmerchanttradePayCloseTest")
public class WsbankBkmerchanttradeRefundQueryTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;
    
    /**
     * 退款查询
     */
    @Test
    public void bkmerchanttradeRefundQuery() {
    	WsbankBkmerchanttradeRefundQueryRequestBody requestBody = new WsbankBkmerchanttradeRefundQueryRequestBody();
    	requestBody.setMerchantId("226801000000103460124");
    	requestBody.setOutRefundNo("o18060116431384520001");//退款交易号
        WsbankBkmerchanttradeRefundQueryRequestInfo requestInfo = new WsbankBkmerchanttradeRefundQueryRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankBkmerchanttradeRefundQueryRequest request = new WsbankBkmerchanttradeRefundQueryRequest();
        request.setRequestInfo(requestInfo);
        WsbankBkmerchanttradeRefundQueryResponse response = wsbankApiService.bkmerchanttradeRefundQuery(request);
        System.out.println("测试退款查询："+ JSON.toJSONString(response));
    }
}
