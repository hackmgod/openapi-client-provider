/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 上海银行错误码  有些业务场景需要根据code做相应的逻辑处理
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum BoscErrorCodeEnum implements Messageable {
	SYSTEM_ERROR("100001","系统错误,联系上海银行处理"),
	PARAMATER_FORMAT_ERROR("100002","json参数格式错误"),
	SIGN_VERIFY_ERROR("100003","签名验证失败"),
	PLATFORMNO_NON_EXIST("100004","平台编号不存在,检查平台编号是否正确"),
	PLATFORM_STATUS_ERROR("100005","平台状态异常,联系上海银行检查平台编号状态"),
	PLATFORM_BIZ_NO_ACCESS("100006","业务未开通,提供平台编号请上海银行协助检查业务开通情况"),
	QUERY_TARGET_NON_EXIST("100007","查询对象不存在"),
	BIZ_ACCEPT_FAIL("100008","业务受理失败"),
	USER_NON_EXIST("100009","用户不存在"),
	USER_ACCOUNT_DISABLED("100010","用户账户不可用"),
	USER_NO_PERMISSION("100011","该用户无此操作权限"),
	UNSUPPORTED_BANK("100012","非常抱歉，暂不支持此银行"),
	REQUESTNO_DUPLICATE("100013","请求流水号重复"),
	BALANCE_INSUFFICIENT("100014","余额不足"),
	TENDER_STATUS_ERROR("100015","标的状态与业务不匹配"),
	PLATFORM_PREPAYMENT_ACCOUNT_BALANCE_INFUFFICIENT("100016","平台垫资账户可用余额不足，请联系平台处理"),
	PLATFORM_NO_PREPAYEMNT_ACCOUNT("100017","平台未开通垫资账户，请联系平台处理"),
	NO_PUBLIC_WITHDRAW_TIME("100018","当前时间不受理加急对公提现"),
	TENDER_TYPE_ERROR("100019","标的类型与业务不匹配"),
	ORDER_PROCESSING("100020","交易处理中，请勿重试");
	
	private final String code;
	private final String message;
	
	private BoscErrorCodeEnum (String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String code() {
		return code;
	}
	
	public String message() {
		return message;
	}
	
	public static Map<String, String> mapping() {
		Map<String, String> map = Maps.newLinkedHashMap();
		for (BoscErrorCodeEnum type : values()) {
			map.put(type.getCode(), type.getMessage());
		}
		return map;
	}
	
	/**
	 * 通过枚举值码查找枚举值。
	 *
	 * @param code 查找枚举值的枚举值码。
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
	 */
	public static BoscErrorCodeEnum find(String code) {
		for (BoscErrorCodeEnum status : values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		throw new IllegalArgumentException("BoscAuditStatusEnum not legal:" + code);
	}
	
	/**
	 * 获取全部枚举值。
	 *
	 * @return 全部枚举值。
	 */
	public static List<BoscErrorCodeEnum> getAll() {
		List<BoscErrorCodeEnum> list = new ArrayList<BoscErrorCodeEnum> ();
		for (BoscErrorCodeEnum status : values()) {
			list.add(status);
		}
		return list;
	}
	
	/**
	 * 获取全部枚举值码。
	 *
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode() {
		List<String> list = new ArrayList<String>();
		for (BoscErrorCodeEnum status : values()) {
			list.add(status.code());
		}
		return list;
	}
		
	}
