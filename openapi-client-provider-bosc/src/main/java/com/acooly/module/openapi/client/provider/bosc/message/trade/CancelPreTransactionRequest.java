package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.CANCEL_PRE_TRANSACTION, type = ApiMessageType.Request)
public class CancelPreTransactionRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 预处理业务流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String preTransactionNo;
	/**
	 * 取消金额
	 */
	@MoneyConstraint(min = 1)
	private Money amount;
	
	public CancelPreTransactionRequest () {
		setService (BoscServiceNameEnum.CANCEL_PRE_TRANSACTION.code ());
	}
	
	public CancelPreTransactionRequest (String requestNo, String preTransactionNo, Money amount) {
		this ();
		this.requestNo = requestNo;
		this.preTransactionNo = preTransactionNo;
		this.amount = amount;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPreTransactionNo () {
		return preTransactionNo;
	}
	
	public void setPreTransactionNo (String preTransactionNo) {
		this.preTransactionNo = preTransactionNo;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
}