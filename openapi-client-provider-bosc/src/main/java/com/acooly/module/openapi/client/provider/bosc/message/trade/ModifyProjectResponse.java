package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscProjectStatusEnum;

import javax.validation.constraints.NotNull;

@ApiMsgInfo(service = BoscServiceNameEnum.MODIFY_PROJECT, type = ApiMessageType.Response)
public class ModifyProjectResponse extends BoscResponse {
	/**
	 * 见【标的状态】
	 */
	@NotNull
	private BoscProjectStatusEnum projectStatus;
	
	public BoscProjectStatusEnum getProjectStatus () {
		return projectStatus;
	}
	
	public void setProjectStatus (BoscProjectStatusEnum projectStatus) {
		this.projectStatus = projectStatus;
	}
}