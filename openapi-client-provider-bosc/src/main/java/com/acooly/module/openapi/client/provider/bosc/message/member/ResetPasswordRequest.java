package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.RESET_PASSWORD, type = ApiMessageType.Request)
public class ResetPasswordRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 变更类型：Remember 为记得密码，主动修改密码；Forget 为忘记密码，重新设置密码； 默认不传 用户自己选择
	 */
	private String isSkip;
	/**
	 * 页面回跳 URL
	 */
	@NotEmpty
	@Size(max = 100)
	private String redirectUrl;
	
	public ResetPasswordRequest () {
		setService (BoscServiceNameEnum.RESET_PASSWORD.code ());
	}
	
	public ResetPasswordRequest (String requestNo, String platformUserNo, String redirectUrl) {
		this();
		this.requestNo = requestNo;
		this.platformUserNo = platformUserNo;
		this.redirectUrl = redirectUrl;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getIsSkip () {
		return isSkip;
	}
	
	public void setIsSkip (String isSkip) {
		this.isSkip = isSkip;
	}
	
	public String getRedirectUrl () {
		return redirectUrl;
	}
	
	public void setRedirectUrl (String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
}