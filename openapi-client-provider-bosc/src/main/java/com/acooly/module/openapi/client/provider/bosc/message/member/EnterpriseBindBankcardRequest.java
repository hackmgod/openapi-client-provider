package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.ENTERPRISE_BIND_BANKCARD, type = ApiMessageType.Request)
public class EnterpriseBindBankcardRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 页面回跳 URL
	 */
	@NotEmpty
	@Size(max = 100)
	private String redirectUrl;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 银行账户号
	 */
	@NotEmpty
	@Size(max = 50)
	private String bankcardNo;
	/**
	 * 见【银行编码】
	 */
	@NotNull
	private BoscBankcodeEnum bankcode;
	/**
	 * 换卡的时候传递  UPDATE_BANKCARD  重新绑卡不填
	 */
	private String bindType;
	
	public EnterpriseBindBankcardRequest () {
		setService (BoscServiceNameEnum.ENTERPRISE_BIND_BANKCARD.code ());
	}
	
	
	public EnterpriseBindBankcardRequest (String requestNo, String redirectUrl, String platformUserNo,
	                                      String bankcardNo,
	                                      BoscBankcodeEnum bankcode) {
		this ();
		this.requestNo = requestNo;
		this.redirectUrl = redirectUrl;
		this.platformUserNo = platformUserNo;
		this.bankcardNo = bankcardNo;
		this.bankcode = bankcode;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getRedirectUrl () {
		return redirectUrl;
	}
	
	public void setRedirectUrl (String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getBankcardNo () {
		return bankcardNo;
	}
	
	public void setBankcardNo (String bankcardNo) {
		this.bankcardNo = bankcardNo;
	}
	
	public BoscBankcodeEnum getBankcode () {
		return bankcode;
	}
	
	public void setBankcode (BoscBankcodeEnum bankcode) {
		this.bankcode = bankcode;
	}
	
	public String getBindType () {
		return bindType;
	}
	
	public void setBindType (String bindType) {
		this.bindType = bindType;
	}
}