package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/26 13:56
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.REFUND,type = ApiMessageType.Response)
@XStreamAlias("result")
public class BaoFuRefundResponse extends BaoFuResponse{

    /**
     * 请求方保留域
     */
    @XStreamAlias("req_reserved")
    private String reqReserved;

    /**
     * 附加字段
     */
    @XStreamAlias("additional_info")
    private String additionalInfo;

    /**
     * 退款宝付业务流水号
     * 宝付返回给商户的唯一退款订单号
     */
    @XStreamAlias("refund_business_no")
    private String refundBusinessNo;

    /**
     * 退款商户订单号
     */
    @XStreamAlias("refund_order_no")
    private String refundOrderNo;

    /**
     * 退款金额
     * 单位：分
     */
    @XStreamAlias("refund_amt")
    private String refundAmt;



}
