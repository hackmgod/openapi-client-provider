package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuDataTypeEnum;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/1 17:33
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.ACCOUNT_BALANCE_QUERY,type = ApiMessageType.Request)
public class BaoFuAccountBalanceQueryRequest extends BaoFuRequest{

    /**
     * 加密数据类型
     */
    @BaoFuAlias(value = "return_type", sign = false)
    private String returnType = BaoFuDataTypeEnum.xml.getCode();

    /**
     * 账户类型
     * 0:全部；
     * 1:基本账户;
     * 2:未结算账户;
     * 3:冻结账户;
     * 4:保证金账户;
     * 5:资金托管账户；
     * 7:手续费账户
     * 9: 资金存管账户
     */
    @BaoFuAlias(value = "account_type", sign = false)
    private String accountType ="0";

}
