<!-- title: 宝付支付SDK组件 -->
<!-- type: gateway -->
<!-- author: zhike -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-baofu</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

直接采用baofu的SDK，然后扩展异步通知处理
使用方式：
1、在自己工程注入BaoFuApiService

2、此sdk提供宝付单笔代扣、单笔代扣查询、批量代扣、批量代扣单笔订单查询、提现、提现查询、对账下载

3、密钥加载支持两种方式
   a：通过配置文件
   配置密钥加载路径；
   b: BaoFuDefaultLoadKeyStoreService，通过传入一个标识性字符串如（partnerId）加载公钥、私钥路径以及私钥密码
   c: 因为银盛接口比较坑异步通知的时候除了订单号没有返回接口任何其它唯一表示字段，如果是托管模式，无法知道此笔异步通知的密钥，所以此sdk提供了
      接口BaoFuDefaultLoadKeyStoreService通过传入订单号，获取此笔订单对应银盛的partnerId，在加上provider标识，获取当前的密钥对象，用于
      异步通知验签

4、本sdk中网关支付和快捷支付的异步通知地址固定值，所以调用的时候不需要传入notify_url：请参考com.acooly.module.openapi.client.provider.baofu.OpenAPIClientBaoFuConfigration.getApiSDKServlet
   a:网关支付：/gateway/notify/baofuNotify/deduct
   b:快捷支付：/gateway/notify/baofuNotify/batchDeduct
   接收异步通知的时候实现接口NotifyHandler，重新handleNotify和serviceKey方法，其中derviceKey方法需要绑定的key值在枚举YinShengServiceEnum可以获取到
   快捷支付异步通知接收示例：
  @Service
  @Slf4j
  public class BaoFuDeductNotifyService implements NotifyHandler {

      @Override
      public void handleNotify(ApiMessage notify) {
          BaoFuDeductNotify baoFuDeductNotify = (BaoFuDeductNotify) notify;

          log.info("接收宝付单笔代扣异步通知通知开始：baoFuDeductNotify：" + JSON.toJSONString(baoFuDeductNotify));

      }

      @Override
      public String serviceKey() {
          return BaoFuServiceEnum.DEDUCT.getCode();
      }
  }

5、接口的服务码和对应的唯一表示统一放在com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum

注意：调用接口的时候method不用手动传入，如果是配置文件方式partnerId也不用传，sign_type默认为RSA,version默认为4.0.0.0，charset默认为UTF-8
     由于每个接口的请求地址可能不一样，配置文件里面配的地址可能不是当前接口的请求地址，所以SDK是支持手动传入请求地址的，判断逻辑为优先使用传入的请求
     地址，如果传入的请求地址为空，则使用配置文件的请求地址；需要注意的是在请求同步返回的时候如果接收到异常：ApiClientSocketTimeoutException、ApiClientProcessingException
     的时候，同步视为处理中，通过订单调用查询接口确认订单的最终状态


6、宝付接口对应使用的其它信息如终端号SDK也优先使用传入的值，如果传入的值为空，则使用配置文件中的值，尤其对托管模式的接入需要注意

7、配置文件示例：
##baofu
acooly.openapi.client.baofu.enable=true
#单笔代扣测试信息
#单笔代扣、单笔代扣查询请求地址
acooly.openapi.client.baofu.gatewayUrl=https://vgw.baofoo.com/cutpayment/api/backTransRequest
#对账文件请求地址
acooly.openapi.client.baofu.gatewayUrl=https://vgw.baofoo.com/boas/api/fileLoadNewRequest
acooly.openapi.client.baofu.publicKeyPath=classpath:keystore/baofu/siglededuct/public.cer
acooly.openapi.client.baofu.privateKeyPath=classpath:keystore/baofu/siglededuct/private.pfx
acooly.openapi.client.baofu.privateKeyPassword=123456
acooly.openapi.client.baofu.connTimeout=10000
acooly.openapi.client.baofu.readTimeout=30000
acooly.openapi.client.baofu.partnerId=100000276
acooly.openapi.client.baofu.terminalId=100000990
acooly.openapi.client.baofu.filePath=D:/var

#测试宝付批量代扣、批量代扣单笔查询请求
acooly.openapi.client.baofu.gatewayUrl=https://vgw.baofoo.com/batchpay/api/batchTransRequest
acooly.openapi.client.baofu.publicKeyPath=classpath:keystore/baofu/batchdeduct/public.cer
acooly.openapi.client.baofu.privateKeyPath=classpath:keystore/baofu/batchdeduct/private.pfx
acooly.openapi.client.baofu.privateKeyPassword=100000178_204500
acooly.openapi.client.baofu.connTimeout=10000
acooly.openapi.client.baofu.readTimeout=30000
acooly.openapi.client.baofu.partnerId=100000178
acooly.openapi.client.baofu.terminalId=100000916
acooly.openapi.client.baofu.filePath=D:/var